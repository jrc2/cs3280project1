import unittest
from ipv4_matchers import extractIpv4AddressesFrom

class TestExtractIpv4AddressesFrom(unittest.TestCase):
    
    def testExceptionWhenEmptyString(self):
        self.assertRaises(ValueError, extractIpv4AddressesFrom, '')
        
    def testExceptionWhenNoneString(self):
        self.assertRaises(ValueError, extractIpv4AddressesFrom, None)
        
    def testJustIpInString(self):
        actual = extractIpv4AddressesFrom('192.168.1.2')
        expected = ['192.168.1.2']
        self.assertEqual(actual, expected)
        
    def testManyIpsInSentence(self):
        actual = extractIpv4AddressesFrom("192.168.1.2 is my IP. Bob's is 192.111.222.33 and my old one is 234.45.67.0.")
        expected = ['192.168.1.2', '192.111.222.33', '234.45.67.0']
        self.assertEqual(actual, expected)
        
    def testNoIpsInString(self):
        actual = extractIpv4AddressesFrom('this is a test')
        expected = []
        self.assertEqual(actual, expected)