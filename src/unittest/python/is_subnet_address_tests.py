import unittest
from ipv4_matchers import isNetmask

class TestIsNetmask(unittest.TestCase):
    
    def testExceptionWhenEmptyString(self):
        self.assertRaises(ValueError, isNetmask, '')
        
    def testExceptionWhenNoneString(self):
        self.assertRaises(ValueError, isNetmask, None)
        
    def testShouldAllowAllOctets255(self):
        self.assertTrue(isNetmask('255.255.255.255'))
        
    def testhouldAllow128In4thOctet(self):
        self.assertTrue(isNetmask('255.255.255.128'))
        
    def testhouldAllow192In4thOctet(self):
        self.assertTrue(isNetmask('255.255.255.192'))
        
    def testhouldAllow224In4thOctet(self):
        self.assertTrue(isNetmask('255.255.255.224'))
        
    def testhouldAllow240In4thOctet(self):
        self.assertTrue(isNetmask('255.255.255.240'))
        
    def testhouldAllow248In4thOctet(self):
        self.assertTrue(isNetmask('255.255.255.248'))
        
    def testhouldAllow252In4thOctet(self):
        self.assertTrue(isNetmask('255.255.255.252'))
        
    def testhouldAllow254In4thOctet(self):
        self.assertTrue(isNetmask('255.255.255.254'))
        
    def testShouldAllow0In3rdOctet(self):
        self.assertTrue(isNetmask('255.255.0.0'))
        
    def testShouldAllow128In3rdOctet(self):
        self.assertTrue(isNetmask('255.255.128.0'))
        
    def testShouldAllow192In3rdOctet(self):
        self.assertTrue(isNetmask('255.255.192.0'))
        
    def testShouldAllow224In3rdOctet(self):
        self.assertTrue(isNetmask('255.255.224.0'))
        
    def testShouldAllow240In3rdOctet(self):
        self.assertTrue(isNetmask('255.255.240.0'))
        
    def testShouldAllow248In3rdOctet(self):
        self.assertTrue(isNetmask('255.255.248.0'))
        
    def testShouldAllow252In3rdOctet(self):
        self.assertTrue(isNetmask('255.255.252.0'))
        
    def testShouldAllow254In3rdOctet(self):
        self.assertTrue(isNetmask('255.255.254.0'))
        
    def testShouldAllow255In3rdOctet(self):
        self.assertTrue(isNetmask('255.255.255.0'))
        
    def testShouldAllow0In2ndOctet(self):
        self.assertTrue(isNetmask('255.0.0.0'))
        
    def testShouldAllow128In2ndOctet(self):
        self.assertTrue(isNetmask('255.128.0.0'))
        
    def testShouldAllow192In2ndOctet(self):
        self.assertTrue(isNetmask('255.192.0.0'))
        
    def testShouldAllow224In2ndOctet(self):
        self.assertTrue(isNetmask('255.224.0.0'))
        
    def testShouldAllow240In2ndOctet(self):
        self.assertTrue(isNetmask('255.240.0.0'))
        
    def testShouldAllow248In2ndOctet(self):
        self.assertTrue(isNetmask('255.248.0.0'))
        
    def testShouldAllow252In2ndOctet(self):
        self.assertTrue(isNetmask('255.252.0.0'))
        
    def testShouldAllow254In2ndOctet(self):
        self.assertTrue(isNetmask('255.254.0.0'))
        
    def testShouldAllow0InFirstOctet(self):
        self.assertTrue(isNetmask('0.0.0.0'))
        
    def testShouldAllow128InFirstOctet(self):
        self.assertTrue(isNetmask('128.0.0.0'))
        
    def testShouldAllow192InFirstOctet(self):
        self.assertTrue(isNetmask('192.0.0.0'))
        
    def testShouldAllow224InFirstOctet(self):
        self.assertTrue(isNetmask('224.0.0.0'))
        
    def testShouldAllow240InFirstOctet(self):
        self.assertTrue(isNetmask('240.0.0.0'))
        
    def testShouldAllow248InFirstOctet(self):
        self.assertTrue(isNetmask('248.0.0.0'))
        
    def testShouldAllow252InFirstOctet(self):
        self.assertTrue(isNetmask('252.0.0.0'))
        
    def testShouldAllow254InFirstOctet(self):
        self.assertTrue(isNetmask('254.0.0.0'))
        
    def testShouldNotAllowNonZeroAfterZero(self):
        self.assertFalse(isNetmask('0.255.0.0'))
        
    def testShouldNotAllowLessThan255Before255(self):
        self.assertFalse(isNetmask('192.255.0.0'))
        
    def testShouldNotAllowTwoNon255Octets(self):
        self.assertFalse(isNetmask('255.192.128.0'))
        
    def testShouldNotAllowInvalidOctet(self):
        self.assertFalse(isNetmask('255.180.0.0'))