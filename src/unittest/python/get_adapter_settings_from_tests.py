import unittest
import textwrap
from ipv4_matchers import getAdapterSettingsFrom

class TestGetAdapterSettingsFrom(unittest.TestCase):
    
    def testExceptionWhenEmptyString(self):
        self.assertRaises(ValueError, getAdapterSettingsFrom, '')
        
    def testExceptionWhenNoneString(self):
        self.assertRaises(ValueError, getAdapterSettingsFrom, None)
    
    def testStringWithSomeValidPairs(self):
        multilineString = textwrap.dedent('''\
            192.168.1.1,255.255.255.0
            hello world!
            160.25.10.7,255.192.0.0
            160.324.10.7,255.192.0.0''')
        
        actual = getAdapterSettingsFrom(multilineString)
        expected = { '192.168.1.1' : '255.255.255.0', '160.25.10.7' : '255.192.0.0' }
        self.assertEqual(actual, expected)
        
    def testStringWithNoValidPairs(self):
        string = '160.324.10.7,255.192.0.0'
        actual = getAdapterSettingsFrom(string)
        expected = {}
        self.assertEqual(actual, expected)
        
    def testValidIpInvalidNetmask(self):
        multilineString = textwrap.dedent('''\
            192.168.1.1,255.192.255.0
            hello world!''')
        
        actual = getAdapterSettingsFrom(multilineString)
        expected = {}
        self.assertEqual(actual, expected)
        
    def testValidNetmaskInvalidIp(self):
        multilineString = textwrap.dedent('''\
            192.365.1.1,255.255.0.0
            hello world!''')
        
        actual = getAdapterSettingsFrom(multilineString)
        expected = {}
        self.assertEqual(actual, expected)