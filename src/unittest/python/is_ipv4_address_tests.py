import unittest
from ipv4_matchers import isIpv4Address

class TestIsIPv4Address(unittest.TestCase):
    
    def testEmptyAddressException(self):
        self.assertRaises(ValueError, isIpv4Address, '')
        
    def testNoneAddressException(self):
        self.assertRaises(ValueError, isIpv4Address, None)
        
    def testShouldAllowIPAllThreeDigits(self):
        self.assertTrue(isIpv4Address('192.192.192.192'))
        
    def testShouldNotAllowExtraDigitsAtEnd(self):
        self.assertFalse(isIpv4Address('192.192.192.1922222'))
        
    def testShouldNotAlloeExtraDigitsAtFront(self):
        self.assertFalse(isIpv4Address('111111111.222.333.444'))
    
    def testAllZeros(self):
        self.assertTrue(isIpv4Address('000.000.000.000'))
        
    def testTwoDigitsPerOctet(self):
        self.assertTrue(isIpv4Address('00.11.22.33'))
        
    def testOneDigitPerOctet(self):
        self.assertTrue(isIpv4Address('1.2.3.4'))
        
    def testHigherThan255(self):
        self.assertFalse(isIpv4Address('256.0.0.0'))
        
    def Test4DigitsInOctet(self):
        self.assertFalse(isIpv4Address('192.1111.1.254'))
        
    def TestOneOctet(self):
        self.assertFalse(isIpv4Address('192'))
        
    def TestTwoOctets(self):
        self.assertFalse(isIpv4Address('192.168'))
        
    def TestThreeOctets(self):
        self.assertFalse(isIpv4Address('192.168.11'))