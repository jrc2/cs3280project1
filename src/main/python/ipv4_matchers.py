import re

ipv4Regex = re.compile(r'''(
    (?:(?:25[0-5])|(?:2[0-4][0-9])|(?:[01]?[0-9]{1,2}))\.   # 1-3 digits from 0-250, followed by a period
    (?:(?:25[0-5])|(?:2[0-4][0-9])|(?:[01]?[0-9]{1,2}))\.   # 1-3 digits from 0-250, followed by a period
    (?:(?:25[0-5])|(?:2[0-4][0-9])|(?:[01]?[0-9]{1,2}))\.   # 1-3 digits from 0-250, followed by a period
    (?:(?:25[0-5])|(?:2[0-4][0-9])|(?:[01]?[0-9]{1,2}))     # 1-3 digits from 0-250
    )''', re.VERBOSE)

netmaskRegex = re.compile(r'''(
    (255\.255\.255\.(0|(128)|(192)|(224)|(240)|(248)|(252)|(254)|(255)))|   # any correct number in 4th octet
    (255.255.(0|(128)|(192)|(224)|(240)|(248)|(252)|(254)|(255)).0)|        # any correct number in 3rd, zero in 4th
    (255.(0|(128)|(192)|(224)|(240)|(248)|(252)|(254)|(255)).0.0)|          # any correct number in 2nd, zero in 3rd and 4th
    ((0|(128)|(192)|(224)|(240)|(248)|(252)|(254)|(255))\.0\.0\.0)          # any correct number in 1st, zero in rest
    )''', re.VERBOSE)

def isIpv4Address(address):
    if address == '':
        raise ValueError('address cannot be empty')
    
    if address == None:
        raise ValueError('address cannot be None')
    
    return ipv4Regex.fullmatch(address)

def extractIpv4AddressesFrom(string):
    if string == '':
        raise ValueError('string cannot be empty')
    
    if string == None:
        raise ValueError('string cannot be None')
    
    return re.findall(ipv4Regex, string)

def isNetmask(address):
    if address == '':
        raise ValueError('address cannot be empty')
    
    if address == None:
        raise ValueError('address cannot be None')
    
    return netmaskRegex.fullmatch(address)

def getAdapterSettingsFrom(multilineString):
    if multilineString == '':
        raise ValueError('string cannot be empty')
    
    if multilineString == None:
        raise ValueError('string cannot be None')
    
    stringLines = multilineString.splitlines()
    allPairs = {}
    for line in stringLines:
        ipv4Address = ''
        netmask = ''
        pair = line.split(',', 2)
        if ipv4Regex.fullmatch(pair[0]) and netmaskRegex.fullmatch(pair[1]):
            ipv4Address = pair[0]
            netmask = pair[1]
            allPairs[ipv4Address] = netmask
    return allPairs